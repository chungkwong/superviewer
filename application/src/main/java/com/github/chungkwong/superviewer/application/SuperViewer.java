/*
 * Copyright (C) 2018 kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.superviewer.application;
import com.github.chungkwong.superviewer.api.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import org.osgi.framework.*;
import org.osgi.framework.launch.*;
/**
 *
 * @author kwong
 */
public class SuperViewer extends Application{
	@Override
	public void start(Stage stage) throws Exception{
		stage.setScene(new Scene(getPane()));
		stage.setMaximized(true);
		stage.show();
	}
	private BorderPane getPane(){
		BorderPane pane=new BorderPane();
		Button open=new Button("打开");
		pane.setTop(open);
		Label status=new Label();
		pane.setBottom(status);
		TabPane content=new TabPane();
		open.setOnAction((e)->{
			open(new FileChooser().showOpenDialog(null),content,status);
		});
		pane.setCenter(content);
		return pane;
	}
	private void open(File file,TabPane pane,Label status){
		if(file!=null){
			try{
				Collection<ServiceReference<ViewerFactory>> serviceReferences=Activator.bundleContext.getServiceReferences(ViewerFactory.class,null);
				Iterator<ServiceReference<ViewerFactory>> applicatable=serviceReferences.stream().filter((ref)->{
					ViewerFactory factory=Activator.bundleContext.getService(ref);
					boolean supported=factory.isViewable(file);
					Activator.bundleContext.ungetService(ref);
					return supported;
				}).iterator();
				while(applicatable.hasNext()){
					ServiceReference<ViewerFactory> ref=applicatable.next();
					try{
						Tab tab=new Tab(file.getName(),Activator.bundleContext.getService(ref).getViewer(file));
						pane.getTabs().add(tab);
						pane.getSelectionModel().select(tab);
						status.setText("成功打开"+file);
						return;
					}catch(Exception ex){
						Logger.getLogger(SuperViewer.class.getName()).log(Level.SEVERE,null,ex);
					}finally{
						Activator.bundleContext.ungetService(ref);
					}
				}
				status.setText("无法打开"+file);
			}catch(InvalidSyntaxException ex){
				Logger.getLogger(SuperViewer.class.getName()).log(Level.SEVERE,null,ex);
			}
		}
	}
	@Override
	public void stop() throws Exception{
		for(Bundle bundle:Activator.bundleContext.getBundles()){
			if(bundle instanceof Framework){
				bundle.stop();
			}
		}
	}
}
