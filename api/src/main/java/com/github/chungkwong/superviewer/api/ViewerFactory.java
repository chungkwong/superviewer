/*
 * Copyright (C) 2018 kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.superviewer.api;
import java.io.*;
import javafx.scene.*;
/**
 * 文件查看器工厂
 *
 * @author kwong
 */
public interface ViewerFactory{
	/**
	 * 打开指定文件
	 *
	 * @param file 要打开的文件
	 * @return 查看器
	 * @throws Exception
	 */
	Node getViewer(File file) throws Exception;
	/**
	 * 判断本工厂是否可能打开指定文件
	 *
	 * @param file 要打开的文件
	 * @return 判断
	 */
	boolean isViewable(File file);
}
