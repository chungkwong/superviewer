/*
 * Copyright (C) 2018 Chan Chung Kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.superviewer.pdf;
import com.github.chungkwong.superviewer.api.*;
import java.io.*;
import java.util.logging.*;
import javafx.embed.swing.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.rendering.*;
/**
 *
 * @author Chan Chung Kwong
 */
public class PdfViewerFactory implements ViewerFactory{
	@Override
	public Node getViewer(File file) throws Exception{
		PDDocument document=PDDocument.load(file);
		PDFRenderer renderer=new PDFRenderer(document);
		Pagination pagination=new Pagination(document.getNumberOfPages(),0);
		pagination.setPageFactory((index)->{
			try{
				return new ImageView(SwingFXUtils.toFXImage(renderer.renderImage(index,1.0f),null));
			}catch(IOException ex){
				Logger.getLogger(PdfViewerFactory.class.getName()).log(Level.SEVERE,null,ex);
				return null;
			}
		});
		return new ScrollPane(pagination);
	}
	@Override
	public boolean isViewable(File file){
		return file.getName().endsWith(".pdf");
		/*try{
			String type=Files.probeContentType(file.toPath());
			return type!=null&&(type.equals("application/pdf"));
		}catch(IOException ex){
			Logger.getLogger(PdfViewerFactory.class.getName()).log(Level.INFO,null,ex);
			return false;
		}*/
	}
}
