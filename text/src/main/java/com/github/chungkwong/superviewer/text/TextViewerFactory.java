/*
 * Copyright (C) 2018 kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.superviewer.text;
import com.github.chungkwong.superviewer.api.*;
import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.logging.*;
import javafx.scene.*;
import javafx.scene.control.*;
/**
 *
 * @author kwong
 */
public class TextViewerFactory implements ViewerFactory{
	@Override
	public Node getViewer(File file) throws Exception{
		return new TextArea(new String(Files.readAllBytes(file.toPath()),StandardCharsets.UTF_8));
	}
	@Override
	public boolean isViewable(File file){
		try{
			String type=Files.probeContentType(file.toPath());
			return type!=null&&(type.startsWith("text/"));
		}catch(IOException ex){
			Logger.getLogger(TextViewerFactory.class.getName()).log(Level.INFO,null,ex);
			return false;
		}
	}
}
