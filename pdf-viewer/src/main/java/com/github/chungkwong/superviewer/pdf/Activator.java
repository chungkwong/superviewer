package com.github.chungkwong.superviewer.pdf;
import com.github.chungkwong.superviewer.api.*;
import java.util.*;
import org.osgi.framework.*;
public class Activator implements BundleActivator{
	public void start(BundleContext context) throws Exception{
		context.registerService(ViewerFactory.class,new PdfViewerFactory(),new Hashtable<String,Object>());
	}
	public void stop(BundleContext context) throws Exception{
	}
}
