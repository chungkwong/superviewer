package com.github.chungkwong.superviewer.application;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
public class Activator implements BundleActivator{
	static BundleContext bundleContext;
	@Override
	public void start(BundleContext context) throws Exception{
		bundleContext=context;
		new Thread(()->SuperViewer.launch(SuperViewer.class)).start();
	}
	@Override
	public void stop(BundleContext context) throws Exception{
	}
}
