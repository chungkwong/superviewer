package com.github.chungkwong.superviewer.image;
import com.github.chungkwong.superviewer.api.*;
import java.util.*;
import org.osgi.framework.*;
public class Activator implements BundleActivator{
	@Override
	public void start(BundleContext context) throws Exception{
		context.registerService(ViewerFactory.class,new ImageViewerFactory(),new Hashtable<String,Object>());
	}
	@Override
	public void stop(BundleContext context) throws Exception{
	}
}
