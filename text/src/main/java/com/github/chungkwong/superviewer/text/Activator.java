package com.github.chungkwong.superviewer.text;
import com.github.chungkwong.superviewer.api.*;
import java.util.*;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
public class Activator implements BundleActivator{
	@Override
	public void start(BundleContext context) throws Exception{
		context.registerService(ViewerFactory.class,new TextViewerFactory(),new Hashtable<String,Object>());
	}
	@Override
	public void stop(BundleContext context) throws Exception{
	}
}
